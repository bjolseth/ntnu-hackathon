# jsxapi on node.js

This example uses jsxapi on node.js to perform a WebRTC call that are supported by Cisco's video systems. The device can stablishes a WebRTC videocall with Google Meet or Microsoft Teams.

## Run example

Edit main.js, set host, username and password for video system.
Also set meetingUrl with the webrtc meeting url and webRTCMeetingtype being either GoogleMeet or MicrosoftTeams.

In order to perform WebRTC calls the device need some specific settings.

``` shell
xConfiguration WebRTC Provider GoogleMeet HostNames: "meet.google.com"
xConfiguration WebRTC Provider GoogleMeet Mode: On
xConfiguration WebRTC Provider MicrosoftTeams Mode: On 
```


``` shell
npm run jsxapi
```


To use other WebRTC webapps the host name needs to be added to the HostNames list. 
For instance to used https://online-voice-recorder.com/ over WebRTC the following settings needs to be set:

``` shell
xConfiguration WebRTC Provider Other AllowInsecureHTTPS: True
xConfiguration WebRTC Provider Other HostNames: "online-voice-recorder.com"
xConfiguration WebRTC Provider Other Mode: On
```