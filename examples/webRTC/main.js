const xapi = require('./xapi');

const codec = {
  host: '',
  username: 'admin',
  password: '',
};

if (!codec.host) {
  console.error('You need to define host, username and password first. Edit main.js');
  process.exit(0);
}

const meetingUrl = 'https://meet.google.com/......' // start a meeting in google meet and use the url here
const webRTCMeetingtype = 'GoogleMeet' // for now 'GoogleMeet' or 'MSTeams'     

xapi.connect(codec);
xapi.webRTCJoin(meetingUrl,webRTCMeetingtype);
setTimeout(() => {
  xapi.hangUp().then(() => process.exit());
}, 30 * 1000);
